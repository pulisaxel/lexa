package com.example.axelpulis;

import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.SearchView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.prefs.PreferenceChangeEvent;

public class SearchActivity extends AppCompatActivity {

    String url;
    private SearchView sv;
    private String searchS;
    private RecyclerView recyclerView;
    private MyAdapter adapter;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private List<ListItem> listItems;
    private String backup ="";
    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        url = "https://wger.de/api/v2/exercise/?format=json&limit=600&language=2";
        sv = findViewById(R.id.searchV);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView2);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(SearchActivity.this));
        listItems = new ArrayList<>();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);


        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                listItems.clear();

                searchS = query;

                new SearchActivity.AsyncHttpTask().execute(url);


                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                backup = newText;
                return false;
            }
        });



    }

    public class AsyncHttpTask extends AsyncTask<String, Void ,String>{
        @Override
        protected String doInBackground(String... urls) {
            String result ="";
            URL url;
            HttpURLConnection urlConnection = null;
            try {
                url = new URL(urls[0]);
                urlConnection =(HttpURLConnection)url.openConnection();
                String response = streamToString(urlConnection.getInputStream());
                parseResult(response);
                return result;


            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }
    }
    String streamToString(InputStream stream) throws IOException
    {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(stream));
        String data;
        String result = "";

        while((data = bufferedReader.readLine()) != null)
        {
           result += data;
        }
        if (null != stream)
        {
            stream.close();
        }

        return result;

    }

    private void parseResult(String result){
        JSONObject response = null;
        try {
            response = new JSONObject(result);
            JSONArray  rows = response.optJSONArray("results");

            for(int i= 0;i < rows.length();i++)
            {
                JSONObject row = rows.optJSONObject(i);
                String name = row.optString("name");


                if (name.contains(searchS))
                {
                    ListItem item = new ListItem(row.getString("name"),row.getString("description"));
                    listItems.add(item);
                }

            }

            adapter = new MyAdapter(listItems,SearchActivity.this);
            recyclerView.setAdapter(adapter);


        } catch (Exception e) {
            e.printStackTrace();
        }




    }
    @Override
    protected void onPause() {
        super.onPause();
        storeData(backup);
    }


    @Override
    protected void onStop() {
        super.onStop();
        storeData(backup);
    }

    public void storeData(String s)
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        editor = sharedPreferences.edit();
        editor.putString("Value",s);
        editor.putString("Date",dateFormat.format(date));
        editor.commit();
    }

    public void loadData()
    {
        String loadText = sharedPreferences.getString("Value","");
        String dateS = sharedPreferences.getString("Date","");
        sv.setQuery(loadText, false);
        View v = findViewById(R.id.activityL);
        Snackbar.make(v,dateS,Snackbar.LENGTH_LONG).setAction("CLOSE",null).show();
    }



    @Override
    protected void onStart() {

        super.onStart();
        loadData();

    }

    @Override
    protected void onResume() {

        super.onResume();
        loadData();


    }


}
