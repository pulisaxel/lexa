package com.example.axelpulis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Pinview pinview = findViewById(R.id.pinView);
        pinview.setPinViewEventListener(new Pinview.PinViewEventListener(){

            public void activityStart(){
                Intent intent = new Intent(getApplicationContext(), FirebaseLogin.class);
                startActivity(intent);
            }

            @Override
            public void onDataEntered(Pinview pinview, boolean b)
            {


                if (pinview.getValue().equals("1234"))
                {
                    Toast.makeText(MainActivity.this,"Pin Correct", Toast.LENGTH_SHORT).show();

                    activityStart();
                }
                else
                {
                    Toast.makeText(MainActivity.this,"Invalid Pin", Toast.LENGTH_SHORT).show();
                }

            }

        });
    }
}
